# Hello-world webapp
This is a simple python app Deployed into kubernetes using built docker image:
-------
# Description

# Requirement  file[text]
it contains required installation to be made once the file is executed in by a docker file.
Flask and requests are contained in the file.

# Docker file [docker]
it contains the image to be pulled and commands to be executed.

# Deployment Yaml file [YAML]
Its a file used to hold deployment configuration for easy automation using k8s.

commands used
To build the image:

# docker build -t hello-webapp:v1 .

To run the container

# docker run -d -p 80:8080 hello-webapp:v1

To test running container

# curl host01

Technology used
python
Docker
kubernetes
